#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <cmath>

using namespace std;

int main(int argc, char const *argv[])
{
	if ( argc != 2 )
    	cout<<"usage: "<< argv[0] <<" <filename>\n";
  	else {
	    ifstream the_file ( argv[1] );
	    if ( !the_file.is_open() )
	    	cout<<"NULL";
	    else {
	    	char x;
	    	string value = "";
	    	while ( the_file.get ( x ) )
	        	value += x;
	        int t = stoi(value);
	        string T = "B";
	        if (t > 4000) {
	        	t = floor (t / 1024);
	        	T = "KiB";
	        }
	     	if (t > 4000) {
	        	t = floor (t / 1024);
	        	T = "MiB";
	        }
	        if (t > 4000) {
	        	t = floor (t / 1024);
	        	T = "GiB";
	        }
	       cout << t << ' ' << T;
	    }
	}
	return 0;
}
